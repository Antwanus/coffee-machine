from data import MENU

resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
}
cash_registry = float(0)
money = {
    "pennies": 0.01,
    "nickles": 0.05,
    "dimes": 0.10,
    "quarters": 0.25,
}


def is_resources_available(item):
    if (resources["water"] - item["ingredients"]["water"]) < 0:
        return "water"
    elif (resources["milk"] - item["ingredients"]["milk"]) < 0:
        return "milk"
    elif (resources["coffee"] - item["ingredients"]["coffee"]) < 0:
        return "coffee"
    else:
        return "all_available"


def reduce_resources(item):
    resources["water"] = resources["water"] - item["ingredients"]["water"]
    resources["milk"] = resources["milk"] - item["ingredients"]["milk"]
    resources["coffee"] = resources["coffee"] - item["ingredients"]["coffee"]


def process_payment(cost):
    print("Please insert coins...")
    quarters_input = int(input("How many quarters?: "))
    dimes_input = int(input("How many dimes?: "))
    nickles_input = int(input("How many nickles?: "))
    pennies_input = int(input("How many pennies?: "))
    total_input = (money["quarters"] * quarters_input) \
                  + (money["dimes"] * dimes_input) \
                  + (money["nickles"] * nickles_input) \
                  + (money["pennies"] * pennies_input)
    return round((total_input - cost), 2)

while True:
    c_choice = input("What would you like to order? [espresso/latte/cappuccino]: ")
    if c_choice == 'report':
        print(f"""
    Water = {resources['water']}
    Milk = {resources['milk']}
    Coffee = {resources['coffee']}
    Cash = ${cash_registry}
    """)
        if input("Continue..."):
            break
    else:
        desired_item = MENU[c_choice]
        print('DEBUG: ingredients', desired_item["ingredients"])
        print('DEBUG: current_resources', resources)
        tmp = is_resources_available(desired_item)
        if tmp == "all_available":
            print(f"All resources are available, proceed with payment (${desired_item['cost']})")
            change = process_payment(desired_item["cost"])
            while change < 0:
                print("That's not enough: ", change)
                change = process_payment(desired_item["cost"])
            cash_registry += desired_item["cost"]
            print(f"Here is ${change} in change.")
            print(f"Enjoy your {c_choice}")
            reduce_resources(desired_item)
            print("DEBUG: new resources = ", resources)
            print("DEBUG: new cash_registry= ", cash_registry)
            if input("Continue... [y/n]") == 'y':
                continue
            else:
                break
        else:
            print(f"Sorry there is not enough {tmp}")
            if input("Continue... [y/n]") == 'y':
                continue
            else:
                break
